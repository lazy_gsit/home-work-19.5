#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "string" << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow" << endl;
    }
};

class Golyb : public Animal
{
public:
    void Voice() override
    {
        cout << "Kyrlik" << endl;
    }
};

int main()
{
    Animal *a[3];
    a[0] = new Dog();
    a[1] = new Cat();
    a[2] = new Golyb();

    for (Animal *i : a)
    {
        i->Voice();
    }
}